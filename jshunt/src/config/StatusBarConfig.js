import { StatusBar } from "react-native";

//Para Android
StatusBar.setBackgroundColor("#DA552F");
//Para IOS
StatusBar.setBarStyle("light-content");