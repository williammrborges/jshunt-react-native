import React from 'react';
import Routes from './routes';
import './config/StatusBarConfig';

const App = () => <Routes />

// Isso de cima é igual a isso de baixo -> Routes
// Como não utiliza outras funcionalidades da de usar
// class App extends Component {
//     render() {
//         return <Routes/>
//     }
// }

export default App;
//Todo componente tem de ter isso
