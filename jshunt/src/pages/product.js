import React from "react";

import { WebView } from "react-native";

//nunca pode ter state

const Product = ({ navigation }) => (
    <WebView source={{uri: navigation.state.params.product.url}} />
);

Product.navigationOptions = ({ navigation }) => ({
    title: navigation.state.params.product.title,
    headerTitleStyle:{
        alignSelf:'center',
        textAlign: 'center',
        width: '80%',
    }
});

export default Product;